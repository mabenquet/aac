#include <iostream>
#include <map>

/****************************************
 * desinfecter                          *
 * Sanitizes a string by removing       *
 * all non-alphabetic characters and    *
 * converting all characters to lower   *
 *                                      *
 * params:                              *
 *      parola: the string to sanitize  *
 ****************************************/
std::string desinfecter(const std::string& parola)
{
    std::string igienizzato;

    // 文字列を走査し、アルファベット以外の文字を取り除き、小文字に変換する
    for (char c : parola) {
        if (isalpha(c)) {
            igienizzato += tolower(c);
        }
    }

    return igienizzato;
}


/****************************************
 * sontDesAnagrammes                    *
 * Checks if two strings are anagrams   *
 *                                      *
 * params:                              *
 *      parola1: the first string       *
 *      parola2: the second string      *
 *                                      *
 * returns:                             *
 *      true if the strings are         *
 *      anagrams, false otherwise       *
 ****************************************/
bool sontDesAnagrammes(const std::string& parola1, const std::string& parola2)
{
    std::map<char, int> contatoreDiCaratteri1;
    std::map<char, int> contatoreDiCaratteri2;

    std::string parolaIgenizzata1 = desinfecter(parola1);
    std::string parolaIgenizzata2 = desinfecter(parola2);

    // 文字列の長さが異なる場合はアナグラムではない
    if (parolaIgenizzata1.length() != parolaIgenizzata2.length()) {
        return false;
    }

    // 文字列の各文字の出現回数を数える
    for (char c : parola1) {
        contatoreDiCaratteri1[c]++;
    }

    for (char c : parola2) {
        contatoreDiCaratteri2[c]++;
    }

    // 2つの文字列の各文字の出現数が同じ場合、それらはアナグラムです
    return contatoreDiCaratteri1 == contatoreDiCaratteri2;
}




int main(int argc, char const* argv[])
{
    if (argc != 3)
    {
        std::cerr << "Verwendung: " << argv[0] << " <wort1> <wort2>" << std::endl;
        return 1;
    }

    std::string parola1 = argv[1];
    std::string parola2 = argv[2];

    std::cout << parola1 << " und " << parola2 << " sind " << (sontDesAnagrammes(parola1, parola2) ? "" : "keine ") << "Anagramm" << std::endl;


    return 0;
}
