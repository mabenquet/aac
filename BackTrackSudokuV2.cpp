#include <cstdio>
#include <vector>
#include <algorithm>
#include <iostream>


// struct décrivant une case dans la grille
typedef struct {
    int i;
    int j;
    int nbValPoss;
} Case;



// Initialisation des tableaux d'existence
void remplirTableaux(bool existeDansLigne[9][9], bool existeDansColonne[9][9], bool existeDansBloc[9][9], int grille[9][9]) {
    int val;
    for(int ligne = 0; ligne < 9; ++ligne) {
        for(int colonne = 0; colonne < 9; ++colonne) {
            val = grille[ligne][colonne];
            if(val != 0) {
                existeDansLigne[ligne][val - 1] = true;
                existeDansColonne[colonne][val - 1] = true;
                existeDansBloc[3*(ligne/3)+(colonne/3)][val - 1] = true;
            }
        }
    }
}



// détermine si val est présent dans grille sur la ligne|colonne|région n°idx (en fonction de mode)
bool estPresent(int grille[9][9], int val, int idx, char mode) {
    // mode = 'L' pour la ligne, 'C' pour la colonne, 'R' pour la region (sous-grille)

    bool estPresent = false;

    int iReg, jReg;

    switch(mode) {
    case 'L':
        for(int col = 0; col < 9 && !estPresent; ++col) if(grille[idx][col] == val) estPresent = true;
        break;
    case 'C':
        for(int lig = 0; lig < 9 && !estPresent; ++lig) if(grille[lig][idx] == val) estPresent = true;
        break;
    case 'R':
        iReg = (idx/3)*3;
        jReg = (idx%3)*3;
        for(int lig = iReg; lig < iReg + 3 && !estPresent; ++lig) {
            for(int col = jReg; col < jReg + 3 && !estPresent; ++col) {
                if(grille[lig][col] == val) estPresent = true;
            }
        }
        break;
    }
    
    return estPresent;
}


// dit si val est possible en i, j dans grille
bool estPossible(int grille[9][9], int val, int i, int j) {
    bool estPossible;
    estPossible = !estPresent(grille, val, i, 'L') && !estPresent(grille, val, j, 'C') && !estPresent(grille, val, ((i/3)*3)+(j/3), 'R');
    return estPossible;
}


// affiche la grille
void printGrille(int grille[9][9]) {
    printf("-------------------------------------\n");
    for(int i = 0; i < 9; ++i) {
        printf("|");
        for(int j = 0; j < 9; ++j) {
            printf(" %c |", grille[i][j]!=0?grille[i][j]+'0':' ');
        }
        printf("\n-------------------------------------\n");
    }
}


// affiche la matrice de présence
void printGrille(bool grille[9][9]) {
    printf("-------------------------------------\n");
    for(int i = 0; i < 9; ++i) {
        printf("|");
        for(int j = 0; j < 9; ++j) {
            printf(" %d |", grille[i][j]);
        }
        printf("\n-------------------------------------\n");
    }
}


// donne le nombre de valeurs possibles en i, j dans grille
int nbValPossibles(int grille[9][9], int i, int j) {
    int nbValPoss = 0;
    for(int nb = 1; nb <= 9; ++nb) {
        if(estPossible(grille, nb, i, j)) ++nbValPoss;
    }
    return nbValPoss;
}



void sortCases(std::vector<Case>& cases) {
    // trie les cases pour que celle avec le moins de possibilités soit à la fin du vecteur
    std::sort(cases.begin(), cases.end(), [](const Case& a, const Case& b) {
        return a.nbValPoss > b.nbValPoss;
    });
}


void majNbValPoss(bool exDansL[9][9], bool exDansC[9][9], bool exDansB[9][9], std::vector<Case>& cases) {
    int compteur;

    for(Case& c: cases) {
        compteur = 0;
        for(int chiffre = 0; chiffre <= 8; ++chiffre) {
            if(!exDansL[c.i][chiffre] && !exDansC[c.j][chiffre] && !exDansB[3*(c.i/3)+(c.j/3)][chiffre]) {
                ++compteur;
            }
        }
        c.nbValPoss = compteur;
    }
}


// trie les cases par nombre de possibilités différentes croissant
std::vector<Case> trierCasesARemplir(int grille[9][9]) {
    std::vector<Case> casesARemplir;
    for(int i = 0; i < 9; ++i) {
        for(int j = 0; j < 9; ++j) {
            if(grille[i][j] == 0) {
                Case c = {i, j, nbValPossibles(grille, i, j)};
                casesARemplir.emplace_back(std::move(c));
            }
        }
    }

    // trie les cases pour que celle avec le moins de possibilités soit à la fin du vecteur
    std::sort(casesARemplir.begin(), casesARemplir.end(), [](const Case& a, const Case& b) {
        return a.nbValPoss > b.nbValPoss;
    });

    return casesARemplir;
}


// FONCTION DE L'ALGO DE RESOLUTION
// fonction récursive pour résoudre la grille
bool estValide(int grille[9][9], std::vector<Case>& cases, bool exDansL[9][9], bool exDansC[9][9], bool exDansB[9][9]) {

    // si fin des cases à remplir, grille remplie
    if(cases.size() == 0) {
        return true;
    }


    // On récupère la dernière case du vecteur, donc celle qui a le moins de choix de chiffres possibles
    std::vector<Case>::iterator it = --cases.end();
    // On la sauvegarde
    Case saveCase = *it;
    // On l'efface de la liste
    cases.pop_back();

    // récupération des coordonnées de la case à remplir
    int ligne = saveCase.i;
    int colonne = saveCase.j;
    int chiffre;


    // on boucle sur les valeurs possibles du sudoku
    for(chiffre = 0; chiffre < 9; ++chiffre) {
        if(!exDansL[ligne][chiffre] && !exDansC[colonne][chiffre] && !exDansB[3*(ligne/3)+(colonne/3)][chiffre]) {
            // s'il n'existe ni dans la ligne, ni dans la colonne, ni dans la région, on le "pré place" dans la grille (on le met dans les matrices d'existence)
            // et on retrie le vecteur des cases à remplir
            exDansL[ligne][chiffre] = exDansC[colonne][chiffre] = exDansB[3*(ligne/3)+(colonne/3)][chiffre] = true;
            majNbValPoss(exDansL, exDansC, exDansB, cases);
            sortCases(cases);

            // on fait un appel récursif pour essayer de résoudre la grille avec la valeur "pré placée"
            if(estValide(grille, cases, exDansL, exDansC, exDansB)) {
                // si la grille est valide, on l'ajoute définitivement
	            grille[ligne][colonne] = chiffre+1;
                return true;
            }
            // sinon, on efface le chiffre et on continue avec les autres valeurs
            exDansL[ligne][chiffre] = exDansC[colonne][chiffre] = exDansB[3*(ligne/3)+(colonne/3)][chiffre] = false;
        }
    }
    return false;
}









int main() {

    bool existeDansLigne[9][9] = {0};
    bool existeDansColonne[9][9] = {0};
    bool existeDansBloc[9][9] = {0};

    int grille[9][9] = {
        {0, 0, 2, 0, 0, 4, 0, 0, 6},
        {0, 0, 0, 0, 7, 0, 3, 4, 0},
        {7, 8, 4, 0, 0, 0, 0, 0, 0},
        {0, 1, 9, 6, 5, 0, 4, 2, 7},
        {2, 7, 0, 0, 0, 9, 8, 3, 0},
        {4, 0, 3, 8, 2, 7, 0, 9, 0},
        {6, 4, 0, 0, 3, 2, 1, 5, 9},
        {1, 0, 0, 9, 4, 8, 0, 0, 0},
        {0, 0, 7, 0, 6, 1, 0, 0, 4}
    };

    // trie les cases par nombre de possibilités différentes croissant
    std::vector<Case> casesARemplir = trierCasesARemplir(grille);

    remplirTableaux(existeDansLigne, existeDansColonne, existeDansBloc, grille);

    printGrille(grille);
    printGrille(existeDansLigne);

    //estValide(grille, casesARemplir.begin(), casesARemplir.end(), existeDansLigne, existeDansColonne, existeDansBloc);
    estValide(grille, casesARemplir, existeDansLigne, existeDansColonne, existeDansBloc);

    printGrille(grille);

    return 0;
}
