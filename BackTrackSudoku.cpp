#include <cstdio>
#include <vector>
#include <algorithm>
#include <iostream>


// struct décrivant une case dans la grille
typedef struct {
    int i;
    int j;
    int nbValPoss;
} Case;



// Initialisation des tableaux d'existence
void remplirTableaux(bool existeDansLigne[9][9], bool existeDansColonne[9][9], bool existeDansBloc[9][9], int grille[9][9]) {
    int val;
    for(int ligne = 0; ligne < 9; ++ligne) {
        for(int colonne = 0; colonne < 9; ++colonne) {
            val = grille[ligne][colonne];
            if(val != 0) {
                existeDansLigne[ligne][val - 1] = true;
                existeDansColonne[colonne][val - 1] = true;
                existeDansBloc[3*(ligne/3)+(colonne/3)][val - 1] = true;
            }
        }
    }
}



// détermine si val est présent dans grille sur la ligne|colonne|région n°idx (en fonction de mode)
bool estPresent(int grille[9][9], int val, int idx, char mode) {
    // mode = 'L' pour la ligne, 'C' pour la colonne, 'R' pour la region (sous-grille)

    bool estPresent = false;

    int iReg, jReg;

    switch(mode) {
    case 'L':
        for(int col = 0; col < 9 && !estPresent; ++col) if(grille[idx][col] == val) estPresent = true;
        break;
    case 'C':
        for(int lig = 0; lig < 9 && !estPresent; ++lig) if(grille[lig][idx] == val) estPresent = true;
        break;
    case 'R':
        iReg = (idx/3)*3;
        jReg = (idx%3)*3;
        for(int lig = iReg; lig < iReg + 3 && !estPresent; ++lig) {
            for(int col = jReg; col < jReg + 3 && !estPresent; ++col) {
                if(grille[lig][col] == val) estPresent = true;
            }
        }
        break;
    }
    
    return estPresent;
}


// dit si val est possible en i, j dans grille
bool estPossible(int grille[9][9], int val, int i, int j) {
    bool estPossible;
    estPossible = !estPresent(grille, val, i, 'L') && !estPresent(grille, val, j, 'C') && !estPresent(grille, val, ((i/3)*3)+(j/3), 'R');
    return estPossible;
}


// affiche la grille
void printGrille(int grille[9][9]) {
    printf("-------------------------------------\n");
    for(int i = 0; i < 9; ++i) {
        printf("|");
        for(int j = 0; j < 9; ++j) {
            printf(" %c |", grille[i][j]!=0?grille[i][j]+'0':' ');
        }
        printf("\n-------------------------------------\n");
    }
}


// affiche la matrice de présence
void printGrille(bool grille[9][9]) {
    printf("-------------------------------------\n");
    for(int i = 0; i < 9; ++i) {
        printf("|");
        for(int j = 0; j < 9; ++j) {
            printf(" %d |", grille[i][j]);
        }
        printf("\n-------------------------------------\n");
    }
}


// donne le nombre de valeurs possibles en i, j dans grille
int nbValPossibles(int grille[9][9], int i, int j) {
    int nbValPoss = 0;
    for(int nb = 1; nb <= 9; ++nb) {
        if(estPossible(grille, nb, i, j)) ++nbValPoss;
    }
    //printf("nbValPoss pour case (%d,%d) : %d\n", i, j, nbValPoss);
    return nbValPoss;
}




// trie les cases par nombre de possibilités différentes croissant
std::vector<Case> trierCasesARemplir(int grille[9][9]) {
    std::vector<Case> casesARemplir;
    for(int i = 0; i < 9; ++i) {
        for(int j = 0; j < 9; ++j) {
            if(grille[i][j] == 0) {
                Case c = {i, j, nbValPossibles(grille, i, j)};
                casesARemplir.emplace_back(std::move(c));
            }
        }
    }

    std::sort(casesARemplir.begin(), casesARemplir.end(), [](const Case& a, const Case& b) {
        return a.nbValPoss < b.nbValPoss;
    });

    return casesARemplir;
}



// FONCTION DE L'ALGO DE RESOLUTION
// fonction récursive pour résoudre la grille
bool estValide(int grille[9][9], std::vector<Case>::iterator it, std::vector<Case>::iterator end, bool exDansL[9][9], bool exDansC[9][9], bool exDansB[9][9]) {

    // si fin des cases à remplir, grille remplie
    if(it == end) {
        return true;
    }


    // récupération des coordonnées de la case à remplir
    int ligne = it->i;
    int colonne = it->j;
    int chiffre;

    // récupération de l'itérateur sur la prochaine case
    std::vector<Case>::iterator prochain = ++it;


    // on boucle sur les valeurs possibles du sudoku
    for(chiffre = 0; chiffre < 9; ++chiffre) {
        if(!exDansL[ligne][chiffre] && !exDansC[colonne][chiffre] && !exDansB[3*(ligne/3)+(colonne/3)][chiffre]) {
            // s'il n'existe ni dans la ligne, ni dans la colonne, ni dans la région, on le "pré place" dans la grille (on le met dans les matrices d'existence)
            exDansL[ligne][chiffre] = exDansC[colonne][chiffre] = exDansB[3*(ligne/3)+(colonne/3)][chiffre] = true;
            // on fait un appel récursif pour essayer de résoudre la grille avec la valeur "pré placée"
            if(estValide(grille, prochain, end, exDansL, exDansC, exDansB)) {
                // si la grille est valide, on l'ajoute définitivement
	            grille[ligne][colonne] = chiffre+1;
                return true;
            }
            // sinon, on l'efface et on continue avec les autres valeurs
            exDansL[ligne][chiffre] = exDansC[colonne][chiffre] = exDansB[3*(ligne/3)+(colonne/3)][chiffre] = false;
        }
    }
    return false;
}









int main() {

    bool existeDansLigne[9][9] = {0};
    bool existeDansColonne[9][9] = {0};
    bool existeDansBloc[9][9] = {0};

    int grille[9][9] = {
        {0, 0, 2, 0, 0, 4, 0, 0, 6},
        {0, 0, 0, 0, 7, 0, 3, 4, 0},
        {7, 8, 4, 0, 0, 0, 0, 0, 0},
        {0, 1, 9, 6, 5, 0, 4, 2, 7},
        {2, 7, 0, 0, 0, 9, 8, 3, 0},
        {4, 0, 3, 8, 2, 7, 0, 9, 0},
        {6, 4, 0, 0, 3, 2, 1, 5, 9},
        {1, 0, 0, 9, 4, 8, 0, 0, 0},
        {0, 0, 7, 0, 6, 1, 0, 0, 4}
    };

    // trie les cases par nombre de possibilités différentes croissant
    std::vector<Case> casesARemplir = trierCasesARemplir(grille);

    remplirTableaux(existeDansLigne, existeDansColonne, existeDansBloc, grille);

    printGrille(grille);
    printGrille(existeDansLigne);

    estValide(grille, casesARemplir.begin(), casesARemplir.end(), existeDansLigne, existeDansColonne, existeDansBloc);

    printGrille(grille);

    return 0;
}
